# Prevent Multiple Form Submissions

Server-side submit request validation prevents multiple forms submits.

Allows setting timeouts for the specific forms which prevent multiple
form submission during that time. Optionally form configuration can be
set to skip timeout if the initial submit request was finished to reduce
waiting time for the next submission for the user. Saet the validation
error message for the situation when multiple submits was detected.

Also, the module provides the possibility to use an API for the custom
controllers for using the configurable locking mechanism. That will help
to keep a single processing operation per session.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/pmfs).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/pmfs).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

Optionally you can also install using composer:
composer require `'drupal/pmfs:1.0.x-dev'`


## Configuration

1. Go to path `'admin/config/system/pmfs'` and it get display PMFS Settings.
2. It has three options that are:
    1. Form Settings
    2. Custom Request Settings
    3. Development Tools


## Maintainers

- Oleh Vehera - [voleger](https://www.drupal.org/u/voleger)
