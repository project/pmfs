<?php

namespace Drupal\Tests\pmfs\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Class MultipleSubmitTest.
 *
 * @package Drupal\Tests\pmfs\Functional
 * @group pmfs
 */
class MultipleSubmitTimeoutTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['pmfs_tests', 'pmfs'];

  /**
   * @var \Drupal\Core\Url|null
   */
  protected $testFormUrl;

  protected $expectedPmfsMessage = 'The form still processing, please, try again later.';

  protected $expectedPmfsTimeout = 10;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('pmfs.settings')
      ->set('form.pmfs_testing_form', [
        'status' => TRUE,
        'timeout' => $this->expectedPmfsTimeout,
        'skip_timeout' => FALSE,
        'message' => $this->expectedPmfsMessage,
        'attach_form_lib' => FALSE,
      ])
      ->save();
    $this->testFormUrl = Url::fromRoute('pmfs.testing');
  }

  /**
   * Test single custom submit handlers trigger.
   */
  public function testSingleCustomSubmit() {
    $condition_of_appeared_form_message = 'document.querySelectorAll("[data-drupal-messages]").length !== 1';
    $css_selector = '[value="Custom submit"]';

    $this->drupalGet($this->testFormUrl);
    $this->click($css_selector);
    $this->getSession()->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains('Form was submitted (custom)');

  }

  /**
   * Test double custom submit handlers trigger.
   */
  public function testDoubleCustomSubmit() {
    $session = $this->getSession();
    $css_selector = '[value="Custom submit"]';
    $expected_submit_message = 'Form was submitted (custom)';
    $condition_of_appeared_form_message = 'document.querySelectorAll("[data-drupal-messages]").length !== 1';

    $this->drupalGet($this->testFormUrl);
    $start = time();
    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_submit_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);

    // Get rid of successful result messages.
    $this->drupalGet($this->testFormUrl);
    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextNotContains($expected_submit_message);
    $this->assertSession()->pageTextContains($this->expectedPmfsMessage);

    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextNotContains($expected_submit_message);
    $this->assertSession()->pageTextContains($this->expectedPmfsMessage);

    // Try to request the page again then click on submit.
    $this->drupalGet($this->testFormUrl);
    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextNotContains($expected_submit_message);
    $this->assertSession()->pageTextContains($this->expectedPmfsMessage);

    // Wait for timeout.
    sleep(max($this->expectedPmfsTimeout - (time() - $start) + 5, 1));

    // Now we can expect submit processing as expected.
    $this->drupalGet($this->testFormUrl);
    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_submit_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);
  }

  /**
   * Test double main submit handlers trigger.
   */
  public function testDoubleMainSubmit() {
    $session = $this->getSession();
    $css_selector = '[value="Main submit"]';
    $expected_submit_message = 'Form was submitted (main)';
    $expected_validation_message = 'Form was validated (main)';
    $condition_of_appeared_form_message = 'document.querySelectorAll("[data-drupal-messages]").length !== 1';

    $this->drupalGet($this->testFormUrl);
    $start = time();
    $this->click($css_selector);
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextContains($expected_submit_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);

    // Get rid of successful result messages.
    $this->drupalGet($this->testFormUrl);
    $this->click($css_selector);
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextNotContains($expected_submit_message);
    $this->assertSession()->pageTextContains($this->expectedPmfsMessage);

    $this->click($css_selector);
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextNotContains($expected_submit_message);
    $this->assertSession()->pageTextContains($this->expectedPmfsMessage);

    // Try to request the page again then click on submit.
    $this->drupalGet($this->testFormUrl);
    $this->click($css_selector);
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextNotContains($expected_submit_message);
    $this->assertSession()->pageTextContains($this->expectedPmfsMessage);

    // Wait for timeout.
    sleep($this->expectedPmfsTimeout - (time() - $start) + 1);

    // Now we can expect submit processing as expected.
    $this->click($css_selector);
    $session->wait(30, $condition_of_appeared_form_message);

    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextContains($expected_submit_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);
  }

  /**
   * Test double custom validation.
   */
  public function testDoubleCustomValidate() {
    $session = $this->getSession();
    $css_selector = '[value="Custom validate"]';
    $expected_validation_message = 'Form was validated (custom)';
    $condition_of_appeared_form_message = 'document.querySelectorAll("[data-drupal-messages]").length !== 1';

    $this->drupalGet($this->testFormUrl);
    $start = time();
    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);

    // Get rid of successful result messages.
    $this->drupalGet($this->testFormUrl);
    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);

    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);

    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);

    // Try to request the page again then click on submit.
    $this->drupalGet($this->testFormUrl);
    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);
    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);

    // Wait for timeout.
    sleep($this->expectedPmfsTimeout - (time() - $start) + 1);

    // Now we can expect submit processing as expected.
    $this->click($css_selector);
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $session->wait(30, $condition_of_appeared_form_message);

    $this->assertSession()->pageTextContains($expected_validation_message);
    $this->assertSession()->pageTextNotContains($this->expectedPmfsMessage);
  }

  /**
   * Test login form process with double submission.
   */
  public function testLoginForm() {
    /*
     * @ToDo: Add scenario for authorization form when user session may be
     *   changed.
     */
    static::assertTrue(TRUE);
  }

}
