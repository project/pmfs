<?php

namespace Drupal\Tests\pmfs\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Class MultipleSubmitTest.
 *
 * @package Drupal\Tests\pmfs\Functional
 * @group pmfs
 */
class SingleSubmitTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['pmfs_tests'];

  /**
   * Make sure that behavior of the custom form is expected.
   */
  public function testIsFormWorkAsExpected() {
    $this->drupalGet(Url::fromRoute('pmfs.testing'));
    $this->click('[value="AJAX Button"]');
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $this->getSession()->wait(30, 'document.querySelectorAll(\'[data-drupal-messages]\').length !== 1');
    $this->assertSession()->pageTextContains('Ajax command triggered');
    $this->click('[value="Custom validate"]');
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $this->getSession()->wait(30, 'document.querySelectorAll(\'[data-drupal-messages]\').length !== 1');
    $this->assertSession()->pageTextContains('Form was validated (custom)');
    $this->click('[value="Custom submit"]');
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $this->getSession()->wait(30, 'document.querySelectorAll(\'[data-drupal-messages]\').length !== 1');
    $this->assertSession()->pageTextContains('Form was submitted (custom)');
    $this->click('[value="Main submit"]');
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $this->getSession()->wait(30, 'document.querySelectorAll(\'[data-drupal-messages]\').length !== 1');
    $this->assertSession()->pageTextContains('Form was validated (main)');
    $this->assertSession()->pageTextContains('Form was submitted (main)');
  }

}
