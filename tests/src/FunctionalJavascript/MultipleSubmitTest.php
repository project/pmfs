<?php

namespace Drupal\Tests\pmfs\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Class MultipleSubmitTest.
 *
 * @package Drupal\Tests\pmfs\Functional
 * @group pmfs
 */
class MultipleSubmitTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['pmfs_tests', 'pmfs'];

  /**
   * Test the same form but with doubling of the requests to the form.
   */
  public function testIsFormWorkAsExpected() {
    /*
     * @ToDo: Add asynchronous double click commands for testing buttons.
     */
    $this->drupalGet(Url::fromRoute('pmfs.testing'));
    $this->click('[value="AJAX Button"]');
    $this->click('[value="AJAX Button"]');
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $this->getSession()->wait(30, 'document.querySelectorAll(\'[data-drupal-messages]\').length !== 1');
    $this->assertSession()->pageTextMatchesCount(1, '#Ajax command triggered#');
    $this->click('[value="Custom validate"]');
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $this->getSession()->wait(30, 'document.querySelectorAll(\'[data-drupal-messages]\').length !== 1');
    $this->assertSession()->pageTextContains('Form was validated (custom)');
    $this->click('[value="Custom submit"]');
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $this->getSession()->wait(30, 'document.querySelectorAll(\'[data-drupal-messages]\').length !== 1');
    $this->assertSession()->pageTextContains('Form was submitted (custom)');
    $this->click('[value="Main submit"]');
    $this->assertSession()->waitForElementVisible('css', '[data-drupal-messages]');
    $this->getSession()->wait(30, 'document.querySelectorAll(\'[data-drupal-messages]\').length !== 1');
    $this->assertSession()->pageTextContains('Form was validated (main)');
    $this->assertSession()->pageTextContains('Form was submitted (main)');
  }

  /**
   * Test login form process with double submission.
   */
  public function testLoginForm() {
    /*
     * @ToDo: Add scenario for authorization form when user session may be
     *   changed.
     */
    static::assertTrue(TRUE);
  }

}
