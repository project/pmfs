<?php

namespace Drupal\pmfs_tests\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TestForm.
 *
 * @package Drupal\pmfs_tests\Form
 */
class TestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pmfs_testing_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['ajax_button'] = [
      '#type' => 'button',
      '#name' => 'ajax',
      '#value' => 'AJAX Button',
      '#ajax' => [
        'callback' => '::ajax',
      ],
    ];
    $form['custom_validate'] = [
      '#type' => 'button',
      '#name' => 'validate_custom',
      '#value' => 'Custom validate',
      '#validate' => ['::validateCustom'],
    ];
    $form['custom_submit'] = [
      '#type' => 'submit',
      '#name' => 'submit_custom',
      '#value' => 'Custom submit',
      '#submit' => ['::submitCustom'],
    ];
    $form['main_submit'] = [
      '#type' => 'submit',
      '#value' => 'Main submit',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addStatus('Form was validated (main)');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addStatus('Form was submitted (main)');
  }

  /**
   * Custom validate callback.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state instance.
   */
  public function validateCustom(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addStatus('Form was validated (custom)');
  }

  /**
   * Custom submit callback.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state instance.
   */
  public function submitCustom(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addStatus('Form was submitted (custom)');
  }

  /**
   * Custom ajax callback.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state instance.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax commands.
   */
  public function ajax(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $response->addCommand(new MessageCommand('Ajax command triggered', NULL, ['type' => 'warning']));
    return $response;
  }

}
