<?php

namespace Drupal\pmfs\EventSubscriber;

use Drupal\Component\Utility\Crypt;
use Drupal\pmfs\Pmfs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Class Cookie.
 *
 * @package Drupal\pmfs\EventSubscriber
 */
class Cookie implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      'kernel.request' => 'setCookie',
    ];
  }

  /**
   * Cookie handler of the module.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Request event.
   */
  public function setCookie(RequestEvent $event): void {
    $request = $event->getRequest();
    if (!$request->cookies->get(Pmfs::COOKIE)) {
      setcookie(Pmfs::COOKIE, Crypt::hashBase64(implode(',', array_merge($request->getClientIps(), [\time()]))), ['path' => '/']);
    }
  }

}
