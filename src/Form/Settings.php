<?php

namespace Drupal\pmfs\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\pmfs\Pmfs;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Settings.
 *
 * @package Drupal\pmfs\Form
 */
class Settings extends ConfigFormBase {

  public const FORM_ID = 'pmfs_settings';

  /**
   * Constructs a \Drupal\pmfs\Form\Settings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The stateinterface object.
   * @param \Drupal\pmfs\Pmfs $pmfs
   *   The pmfs object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected StateInterface $state,
    protected Pmfs $pmfs
  ) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('pmfs')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pmfs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pmfs_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $button = $form_state->getTriggeringElement();
    $configs = $this->configFactory()->getEditable('pmfs.settings');
    $global_id = '_global';
    if (isset($button['#name']) && $button['#name'] === 'add_new') {
      $new_values = $form_state->getValue([
        $button['#ajax']['pmfs_container'],
        $button['#ajax']['pmfs_id'],
      ]);
      $form_state->setCleanValueKeys([
        $button['#ajax']['pmfs_container'],
        $button['#ajax']['pmfs_id'],
      ]);
      $base_key = $button['#ajax']['pmfs_container'] . '.' . $new_values['id'];
      $prev_values = $configs->get($base_key);
      if (!empty($prev_values)) {
        $this->messenger()->addError('New settings already exists in the list');
      }
      else {
        $base_key .= '.';
        $configs
          ->set($base_key . Pmfs::KEY_TIMEOUT, (int) $new_values[Pmfs::KEY_TIMEOUT])
          ->set($base_key . Pmfs::KEY_SKIP_TIMEOUT, (bool) $new_values[Pmfs::KEY_SKIP_TIMEOUT])
          ->set($base_key . Pmfs::KEY_MESSAGE, $new_values[Pmfs::KEY_MESSAGE])
          ->set(
            $base_key . Pmfs::KEY_FORCE_ATTACH_FORM_LIBRARY,
            (bool) $new_values[Pmfs::KEY_FORCE_ATTACH_FORM_LIBRARY]
          )
          ->save();
      }
    }
    if (isset($button['#name']) && strpos($button['#name'], 'remove_item_') === 0) {
      $configs
        ->clear($button['#ajax']['pmfs_container'] . '.' . $button['#ajax']['pmfs_id'])
        ->save();
    }
    $forms = $configs->get('form') ?? [];
    $form['form'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Form settings'),
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'pmfs_settings_form',
      ],
    ];
    if (!isset($forms[$global_id])) {
      $forms = array_merge([$global_id => []], $forms);
    }
    ksort($forms);
    foreach ($forms as $id => $form_settings) {
      $this->buildSettingFormItem($form, 'form', $id, $form_settings, $id !== $global_id);
    }
    $this->buildSettingFormItem($form, 'form', '_new', [], FALSE, TRUE);
    $customs = $configs->get('custom') ?? [];
    $form['custom'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Custom request settings'),
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'pmfs_settings_custom',
      ],
    ];
    if (empty($customs)) {
      $form['custom']['message'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No custom calls detected.'),
      ];
    }
    else {
      ksort($customs);
      foreach ($customs as $id => $custom_settings) {
        $this->buildSettingFormItem($form, 'custom', $id, $custom_settings);
      }
    }
    $dev_mode_enabled = (bool) $this->state->get('pmfs.dev_mode.enabled', FALSE);
    $form['dev'] = [
      '#type' => 'details',
      '#open' => $dev_mode_enabled,
      '#title' => $this->t('Development tools'),
    ];
    $form['dev']['is_development_mode'] = [
      '#type' => 'checkbox',
      '#default_value' => $dev_mode_enabled,
      '#title' => $this->t('Enable module development mode'),
    ];
    $list = $this->state->get('pmfs.dev_mode.detected_form_ids', []);
    $list = array_filter($list, function ($k) use ($forms) {
      return !array_key_exists($k, $forms);
    });
    $list = implode('<br>', $list);
    $form['dev']['detected_ids'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('The list of detected FORM_IDs:'),
      '#access' => $dev_mode_enabled,
      'list' => [
        '#markup' => empty($list) ?
        '< ' . $this->t('Render the form to see the FORM_ID in the list here') . ' >' :
        $list,
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $message = $form_state->getValue(['form', '_global', 'message']);
    $message ?: $form_state->setErrorByName(
      'form][_global][message',
      $this->t(
        'Validation message for FORM ID: _GLOBAL cannot be empty.'
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $config = $this->configFactory->getEditable('pmfs.settings');
    foreach ($form_state->getValue('form', []) as $id => $settings) {
      if ($id === '_new') {
        continue;
      }
      $config->set('form.' . $id, $this->prepareSettingItemValues($settings));
      $this->pmfs->releaseFormLock($id);
    }
    foreach ($form_state->getValue('custom', []) as $id => $settings) {
      $config->set('custom.' . $id, $this->prepareSettingItemValues($settings));
    }
    $config->save();
    $dev_mode = (bool) $form_state->getValue('is_development_mode', FALSE);
    if (!$dev_mode) {
      $this->state->delete('pmfs.dev_mode.detected_form_ids');
    }
    $this->state->set('pmfs.dev_mode.enabled', $dev_mode);
  }

  /**
   * Setting item builder.
   *
   * @param array $container
   *   Container which will place the item.
   * @param string $container_key
   *   Container key.
   * @param string $config_id
   *   Id of the item.
   * @param mixed[] $setting
   *   Settings options.
   * @param bool $remove_controls
   *   Attach or not the remove button.
   * @param bool $add_controls
   *   Attach or not the add button.
   */
  protected function buildSettingFormItem(array &$container, string $container_key, string $config_id, array $setting, bool $remove_controls = TRUE, bool $add_controls = FALSE): void {
    $setting = $this->prepareSettingItemValues($setting);
    $container[$container_key][$config_id] = [
      '#type' => 'details',
      '#title' => $add_controls ? $this->t('New configuration item') : $this->t('Form ID: @id', ['@id' => $config_id]),
      'id' => [
        '#type' => 'textfield',
        '#access' => $add_controls,
        '#title' => $this->t('Form ID'),
        '#description' => $this->t('Set Form ID value to configure setting for the particular form.'),
      ],
      Pmfs::KEY_STATUS => [
        '#type' => 'checkbox',
        '#default_value' => $add_controls ?: $setting[Pmfs::KEY_STATUS],
        '#title' => $this->t('Enabled'),
        '#description' => $this->t('The status of the configuration.'),
      ],
      Pmfs::KEY_TIMEOUT => [
        '#type' => 'number',
        '#default_value' => $setting[Pmfs::KEY_TIMEOUT],
        '#title' => $this->t('Timeout'),
        '#description' => $this->t('Time which have to left before allowed following submit request.'),
      ],
      Pmfs::KEY_SKIP_TIMEOUT => [
        '#type' => 'checkbox',
        '#default_value' => $setting[Pmfs::KEY_SKIP_TIMEOUT],
        '#title' => $this->t('Skip timeout'),
        '#description' => $this->t('If enabled the timout will be skipped if the submit callback was completely processed.'),
      ],
      Pmfs::KEY_MESSAGE => [
        '#type' => 'textfield',
        '#default_value' => $setting[Pmfs::KEY_MESSAGE],
        '#title' => $this->t('Validation message'),
        '#description' => $this->t('The message which would be visible after form submission if previous form submit not completely proceed'),
      ],
      Pmfs::KEY_FORCE_ATTACH_FORM_LIBRARY => [
        '#type' => 'checkbox',
        '#default_value' => $setting[Pmfs::KEY_FORCE_ATTACH_FORM_LIBRARY],
        '#title' => $this->t('Force attach core/drupal.form library'),
        '#description' => $this->t('If enabled the core/drupal.form would be attached for the configured form'),
        '#access' => $container_key !== 'custom',
      ],
    ];
    if ($add_controls) {
      $container[$container_key][$config_id]['add'] = [
        '#type' => 'button',
        '#name' => 'add_new',
        '#value' => $this->t('Save a new item'),
        '#ajax' => [
          'pmfs_id' => $config_id,
          'pmfs_container' => $container_key,
          'wrapper' => 'pmfs_settings_' . $container_key,
          'method' => 'replace',
          'callback' => '::ajaxCallback',
        ],
      ];
    }
    if ($remove_controls) {
      $container[$container_key][$config_id]['remove'] = [
        '#type' => 'button',
        '#name' => 'remove_item_' . $container_key . '_' . $config_id,
        '#value' => $this->t('Remove "@id" configuration', ['@id' => $config_id]),
        '#ajax' => [
          'pmfs_id' => $config_id,
          'pmfs_container' => $container_key,
          'wrapper' => 'pmfs_settings_' . $container_key,
          'method' => 'replace',
          'callback' => '::ajaxCallback',
        ],
      ];
    }
  }

  /**
   * Ajax callback handler.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed
   *   Ajax response.
   */
  public function ajaxCallback(array $form, FormStateInterface $form_state): mixed {
    $button = $form_state->getTriggeringElement();
    return $form[$button['#ajax']['pmfs_container']];
  }

  /**
   * Prepare setting.
   *
   * @param mixed[] $settings
   *   Settings array.
   *
   * @return array
   *   Normalized settings.
   */
  protected function prepareSettingItemValues(array $settings): array {
    $default_values = $this->pmfs->getDefaultValues();
    return [
      Pmfs::KEY_STATUS => (bool) ($settings[Pmfs::KEY_STATUS] ?? $default_values[Pmfs::KEY_STATUS]),
      Pmfs::KEY_TIMEOUT => (int) ($settings[Pmfs::KEY_TIMEOUT] ?? $default_values[Pmfs::KEY_TIMEOUT]),
      Pmfs::KEY_SKIP_TIMEOUT => (bool) ($settings[Pmfs::KEY_SKIP_TIMEOUT] ?? $default_values[Pmfs::KEY_SKIP_TIMEOUT]),
      Pmfs::KEY_MESSAGE => $settings[Pmfs::KEY_MESSAGE] ?? $default_values[Pmfs::KEY_MESSAGE],
      Pmfs::KEY_FORCE_ATTACH_FORM_LIBRARY => (bool) ($settings[Pmfs::KEY_FORCE_ATTACH_FORM_LIBRARY] ??
        $default_values[Pmfs::KEY_FORCE_ATTACH_FORM_LIBRARY]),
    ];
  }

}
