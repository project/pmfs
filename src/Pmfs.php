<?php

namespace Drupal\pmfs;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pmfs\Form\Settings;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Pmfs.
 *
 * @package Drupal\pmfs
 */
class Pmfs {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  public const KEY_TIMEOUT = 'timeout';

  public const KEY_SKIP_TIMEOUT = 'skip_timeout';

  public const KEY_MESSAGE = 'message';

  public const KEY_STATUS = 'status';

  public const COOKIE = 'pmfs_key';

  public const KEY_FORCE_ATTACH_FORM_LIBRARY = 'attach_form_lib';

  /**
   * Static storage of loaded form settings.
   *
   * @var array[]
   */
  protected array $loadedSettings = [];

  /**
   * Static storage of generated keys by the user in current request.
   *
   * @var array[]
   */
  protected array $userGeneratedKeys = [];

  /**
   * Pmfs constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $conf
   *   Configuration factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State storage service.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   Locking service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   */
  public function __construct(
    protected ConfigFactoryInterface $conf,
    protected StateInterface $state,
    protected LockBackendInterface $lock,
    protected MessengerInterface $messenger,
    protected RequestStack $requestStack
  ) {
  }

  /**
   * Main controller method.
   *
   * Defines validation handler for all buttons in the form render array.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FOrm state instance.
   * @param string $form_id
   *   Form ID.
   */
  public function formAlter(array &$form, FormStateInterface $form_state, string $form_id): void {
    if ($form_id === Settings::FORM_ID) {
      return;
    }
    $this->devModeAlter($form, $form_state, $form_id);
    if ($this->isFormEnabled($form_id)) {
      $this->handleLibraryAttachment($form, $form_id);
      $this->prepareCallbacks($form, $form, TRUE);
      $this->formPrepareButtons($form, $form);
    }
  }

  /**
   * Recursively process buttons of the form.
   *
   * @param array $form
   *   Base form render array.
   * @param array $element
   *   Render array.
   */
  protected function formPrepareButtons(array &$form, array &$element): void {
    foreach ($element as $key => &$value) {
      if ($this->elementIsStructure($key, $value)) {
        $this->formPrepareButtons($form, $value);
      }
      if ($this->elementIsButton($value)) {
        $this->prepareCallbacks($form, $value);
      }
    }
  }

  /**
   * Check if provided element is a button.
   *
   * @param array|mixed $element
   *   Render array.
   *
   * @return bool
   *   True if element can submit a form.
   */
  protected function elementIsButton(mixed $element): bool {
    return is_array($element) &&
      isset($element['#type']) &&
      in_array($element['#type'], ['button', 'submit'], TRUE);
  }

  /**
   * Provides required callbacks for submit elements.
   *
   * @param array $form
   *   Base form render array.
   * @param array $element
   *   Render array element to proceed.
   * @param bool $root_call
   *   Flag used if the method called at the root level of the form build.
   */
  protected function prepareCallbacks(array &$form, array &$element, bool $root_call = FALSE): void {
    if (isset($element['#ajax']['callback'])) {
      $element['#ajax']['pmfs_callback'] = $element['#ajax']['callback'];
      $element['#ajax']['callback'] = [$this, 'formAjaxCallback'];
    }
    $is_validate_handlers_set = isset($element['#validate']);
    $is_submit_handlers_set = isset($element['#submit']);
    // Earlier exit case.
    if (!$root_call && !$is_validate_handlers_set && !$is_submit_handlers_set) {
      // This is not a root element call and both handlers collection are empty.
      return;
    }

    // If it's a root element, make sure that validation handler collection
    // is initialized.
    if ($root_call && empty($form['#validate'])) {
      $element['#validate'] = [];
    }
    // Add the PMFS validation handler at the beginning of the validation
    // handlers collection.
    // Non-root elements uses root validation handlers if it has own empty
    // validation handlers collection.
    if ($root_call || (!empty($element['#validate']))) {
      array_unshift($element['#validate'], [$this, 'formValidate']);
    }

    // Add the PMFS submit handler at the end of submit handlers collection.
    if ($root_call || (!empty($element['#submit']))) {
      array_unshift($element['#submit'], [$this, 'formSubmitFirst']);
      $element['#submit'][] = [$this, 'formSubmitLast'];
    }
  }

  /**
   * Check if provided value is the structure if the render elements.
   *
   * @param string $key
   *   Key value of render array.
   * @param mixed $value
   *   Value of array item.
   *
   * @return bool
   *   Flag is value is an array.
   */
  protected function elementIsStructure(string $key, mixed $value): bool {
    return (!str_starts_with($key, '#')) &&
      is_array($value);
  }

  /**
   * Validate form handler.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function formValidate(array $form, FormStateInterface $form_state): void {
    // Check lock.
    if (!$this->lockMayBeAvailableByFormState($form_state)) {
      $this->setLockValidationError($form, $form_state);
    }
  }

  /**
   * Validate form handler.
   *
   * @param string $id
   *   Request id.
   * @param bool $show_message_on_deny
   *   Flag to show error message or not.
   *
   * @return bool
   *   Flag of set lock or not.
   */
  public function isCustomRequestExecutable(string $id, bool $show_message_on_deny = TRUE): bool {
    // Check lock.
    if (!$this->lockMayBeAvailableByCustomId($id)) {
      if ($show_message_on_deny) {
        $this->setCustomLockErrorMessage($id);
      }
      return FALSE;
    }
    // Set lock.
    $this->setLockByCustomId($id);
    return TRUE;
  }

  /**
   * First form submit handler to set lock.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function formSubmitFirst(array $form, FormStateInterface $form_state): void {
    // Set lock.
    $this->setLockByFormState($form_state);
  }

  /**
   * Last form submit handler.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function formSubmitLast(array $form, FormStateInterface $form_state): void {
    // Check lock.
    if ($this->forceReleaseFormLock($form_state) && !$this->lockMayBeAvailableByFormState($form_state)) {
      // Release lock.
      $this->releaseFormLock($form_state);
    }
  }

  /**
   * Custom request finished handler.
   *
   * @param string $id
   *   Custom request ID.
   */
  public function setCustomRequestDone(string $id): void {
    // Check lock.
    if ($this->forceReleaseCustomLock($id) && !$this->lockMayBeAvailableByCustomId($id)) {
      // Release lock.
      $this->releaseCustomLock($id);
    }
  }

  /**
   * Ajax callback handler.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed
   *   Ajax callback result.
   */
  public function formAjaxCallback(array $form, FormStateInterface $form_state): mixed {
    $this->formSubmitFirst($form, $form_state);
    // Check lock.
    if (!$this->lockMayBeAvailableByFormState($form_state)) {
      $this->setLockValidationError($form, $form_state);
      $commands = new AjaxResponse();
      $commands->addCommand(new MessageCommand(
        $this->getFormSettings($form_state)[static::KEY_MESSAGE],
        NULL,
        ['type' => 'error']
      ));
      return $commands;
    }
    // Set lock.
    $this->setLockByFormState($form_state);
    $callback = $form_state->getTriggeringElement()['#ajax']['pmfs_callback'];
    if (str_starts_with($callback, '::')) {
      $callback = [
        $form_state->getFormObject(),
        str_replace('::', '', $callback),
      ];
    }
    return $callback($form, $form_state);
  }

  /**
   * Is form configured for force release option.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return bool
   *   Flag of force release option of specific form configuration.
   */
  public function forceReleaseFormLock(FormStateInterface $form_state): bool {
    return (bool) $this->getFormSettings($form_state)[static::KEY_SKIP_TIMEOUT];
  }

  /**
   * Is custom settings configured for force release option.
   *
   * @param string $id
   *   Form state.
   *
   * @return bool
   *   Flag of force release option of specific custom configuration.
   */
  public function forceReleaseCustomLock(string $id): bool {
    return (bool) $this->getCustomSettings($id)[static::KEY_SKIP_TIMEOUT];
  }

  /**
   * Releases the given lock for the specific form.
   *
   * @param string|\Drupal\Core\Form\FormStateInterface $id
   *   Form state or form ID.
   *
   * @see \Drupal\Core\Lock\LockBackendInterface::release()
   */
  public function releaseFormLock(string|FormStateInterface $id): void {
    $this->lock->release(
      $this->getLockKeyByFormState($id)
    );
  }

  /**
   * Releases the given lock for the specific custom request.
   *
   * @param string $id
   *   Form state or form ID.
   *
   * @see \Drupal\Core\Lock\LockBackendInterface::release()
   */
  public function releaseCustomLock(string $id): void {
    $this->lock->release(
      $this->getCustomLockKey($id)
    );
  }

  /**
   * Validation error message handler for the specific form configuration.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function setLockValidationError(array $form, FormStateInterface $form_state): void {
    $form_state->setError(
      $form,
      $this->getFormSettings($form_state)[static::KEY_MESSAGE]
    );
  }

  /**
   * Validation error message handler for the specific custom configuration.
   *
   * @param string $id
   *   Lock id.
   */
  public function setCustomLockErrorMessage(string $id): void {
    $this->messenger->addError(
      $this->getCustomSettings($id)[static::KEY_MESSAGE]
    );
  }

  /**
   * Acquires a lock for the specific form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @see \Drupal\Core\Lock\LockBackendInterface::acquire()
   */
  public function setLockByFormState(FormStateInterface $form_state): void {
    $this->lock->acquire(
      $this->getLockKeyByFormState($form_state),
      $this->getFormSettings($form_state)[static::KEY_TIMEOUT]
    );
  }

  /**
   * Acquires a lock for the specific custom request.
   *
   * @param string $id
   *   Form state.
   *
   * @see \Drupal\Core\Lock\LockBackendInterface::acquire()
   */
  public function setLockByCustomId(string $id): void {
    $this->lock->acquire(
      $this->getCustomLockKey($id),
      $this->getCustomSettings($id)[static::KEY_TIMEOUT]
    );
  }

  /**
   * Default values getter.
   *
   * @return array
   *   Default values.
   */
  public function getDefaultValues(): array {
    $default_config = $this->conf->get('pmfs.settings')->get('form._global') ?? [];
    return [
      static::KEY_STATUS => (bool) ($default_config[static::KEY_STATUS] ?? FALSE),
      static::KEY_TIMEOUT => (int) ($default_config[static::KEY_TIMEOUT] ?? 30),
      static::KEY_SKIP_TIMEOUT => (bool) ($default_config[static::KEY_SKIP_TIMEOUT] ?? FALSE),
      static::KEY_MESSAGE => $default_config[static::KEY_MESSAGE] ??
      $this->t('The form still processing, please, try again later.'),
      static::KEY_FORCE_ATTACH_FORM_LIBRARY => $default_config[static::KEY_FORCE_ATTACH_FORM_LIBRARY] ?? FALSE,
    ];
  }

  /**
   * Checks if a lock is available for acquiring for the specific form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return bool
   *   Lock availability flag.
   *
   * @see \Drupal\Core\Lock\LockBackendInterface::lockMayBeAvailable()
   */
  protected function lockMayBeAvailableByFormState(FormStateInterface $form_state): bool {
    return $this->lock->lockMayBeAvailable(
      $this->getLockKeyByFormState($form_state)
    );
  }

  /**
   * Checks if a lock is available for acquiring for the specific form.
   *
   * @param string $id
   *   Form state.
   *
   * @return bool
   *   Lock availability flag.
   *
   * @see \Drupal\Core\Lock\LockBackendInterface::lockMayBeAvailable()
   */
  protected function lockMayBeAvailableByCustomId(string $id): bool {
    return $this->lock->lockMayBeAvailable(
      $this->getCustomLockKey($id)
    );
  }

  /**
   * Get ID for the current user.
   *
   * @param string $type
   *   Type of the lock.
   * @param string $id
   *   ID of the lock.
   *
   * @return string
   *   Lock key.
   */
  public function getLockKey(string $type, string $id): string {
    if (!isset($this->userGeneratedKeys[$type][$id])) {
      $this->userGeneratedKeys[$type][$id] = implode('.', [
        'pmfs',
        $type,
        $id,
        $this->requestStack->getCurrentRequest()->cookies->get(static::COOKIE),
      ]);
    }
    return $this->userGeneratedKeys[$type][$id];
  }

  /**
   * Get ID for the current user.
   *
   * @param string|\Drupal\Core\Form\FormStateInterface $id
   *   Form state.
   *
   * @return string
   *   Lock key.
   */
  public function getLockKeyByFormState(string|FormStateInterface $id): string {
    if ($id instanceof FormStateInterface) {
      return $this->getLockKeyByFormState($id->getFormObject()->getFormId());
    }
    return $this->getLockKey('form', $id);
  }

  /**
   * Get custom ID for the current user.
   *
   * @param string $id
   *   Form state.
   *
   * @return string
   *   Lock key.
   */
  public function getCustomLockKey(string $id): string {
    return $this->getLockKey('custom', $id);
  }

  /**
   * Form settings getter.
   *
   * @param string|\Drupal\Core\Form\FormStateInterface $form_id
   *   Form ID or Form State instance.
   *
   * @return array
   *   Default configs.
   */
  public function getFormSettings(string|FormStateInterface $form_id): array {
    if ($form_id instanceof FormStateInterface) {
      return $this->getFormSettings($form_id->getFormObject()->getFormId());
    }
    if (!isset($this->loadedSettings['form'][$form_id])) {
      $settings = $this->conf
        ->get('pmfs.settings')
        ->get('form.' . $form_id) ?? [];
      $settings = array_replace($this->getDefaultValues(), $settings);
      $this->loadedSettings['form'][$form_id] = $settings;
    }
    return $this->loadedSettings['form'][$form_id];
  }

  /**
   * Custom settings getter.
   *
   * @param string $id
   *   Form ID or Form State instance.
   *
   * @return array
   *   Default configs.
   */
  public function getCustomSettings(string $id): array {
    if (!isset($this->loadedSettings['custom'][$id])) {
      $settings = $this->conf
        ->get('pmfs.settings')
        ->get('custom.' . $id) ?? [];
      $create = empty($settings);
      $settings = array_replace($this->getDefaultValues(), $settings);
      if ($create && $this->state->get('pmfs.dev_mode.enabled', FALSE)) {
        $this->conf
          ->getEditable('pmfs.settings')
          ->set('custom.' . $id, $settings);
      }
      $this->loadedSettings['custom'][$id] = $settings;
    }
    return $this->loadedSettings['custom'][$id];
  }

  /**
   * Get flag of form configuration status.
   *
   * @param string $form_id
   *   Form ID.
   *
   * @return bool
   *   Status flag.
   */
  public function isFormEnabled(string $form_id): bool {
    return (bool) $this->getFormSettings($form_id)[static::KEY_STATUS];
  }

  /**
   * Get flag of custom configuration status.
   *
   * @param string $id
   *   Custom request ID.
   *
   * @return bool
   *   Status flag.
   */
  public function isCustomEnabled(string $id): bool {
    return (bool) $this->getCustomSettings($id)[static::KEY_STATUS];
  }

  /**
   * Dev mode features.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param string $form_id
   *   Form ID.
   */
  protected function devModeAlter(array $form, FormStateInterface $form_state, string $form_id): void {
    if ($this->state->get('pmfs.dev_mode.enabled', FALSE)) {
      $list = $this->state->get('pmfs.dev_mode.detected_form_ids', []);
      $list[] = $form_id;
      $this->state->set('pmfs.dev_mode.detected_form_ids', array_unique($list));
    }
  }

  /**
   * Handler of the client side library attachment.
   *
   * Usually this library attached for the drupal form. This helpful for cases
   * when the library not attached for some reason.
   *
   * @param array $form
   * @param $form_id
   */
  protected function handleLibraryAttachment(array &$form, $form_id): void {
    $settings = $this->getFormSettings($form_id);
    if ($settings[static::KEY_FORCE_ATTACH_FORM_LIBRARY]) {
      $form['#attached']['library'][] = 'core/drupal.form';
    }
  }

}
